module Camera()
{
//camera is 25
aCamera = 25;
aCameraEpaisseur = 1;
aCameraHauteur = 9;

aEpaisseurBloc = 2;



difference() {
cube(size = [6,aCamera+2*aEpaisseurBloc,aCameraHauteur], center = false);

translate(v = [-aEpaisseurBloc, 6-aEpaisseurBloc, aEpaisseurBloc])
cube(size = [aCameraHauteur,aCamera-2*aEpaisseurBloc,aCameraHauteur], center = false);
    
translate(v = [aEpaisseurBloc, aEpaisseurBloc, aEpaisseurBloc])
cube(size = [aCameraEpaisseur,aCamera,aCameraHauteur], center = false);
}
}
//Camera();