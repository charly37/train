module TrainBase()
{

//dimension in mm


aDistanceEntreBloqueurs=151;
//la taille exterieur du train
aTrainl=30;
aBlockerSize=5;

aEpaisseurBleue = 4;
translate([-aBlockerSize,aTrainl/2-15/2,0])
color( "MediumBlue" )
cube(size = [2*aBlockerSize+aDistanceEntreBloqueurs,aTrainl/2,aEpaisseurBleue], center = false);

aEpaisseurViolet = 2;
aLargeurViolet = aTrainl+10;
translate([0,-(aLargeurViolet-aTrainl)/2,aEpaisseurBleue])
color( "Violet" )
cube(size = [aDistanceEntreBloqueurs,aLargeurViolet,aEpaisseurViolet], center = false);


//One end   
translate([-aBlockerSize,0,-7]) 
color( "SpringGreen" )
cube(size = [aBlockerSize,aTrainl,aEpaisseurBleue+7+aEpaisseurViolet], center = false);


//another end
translate([aDistanceEntreBloqueurs,0,-7]) 
color( "Red" )
cube(size = [aBlockerSize,aTrainl,aEpaisseurBleue+7+aEpaisseurViolet], center = false);
}

//TrainBase();