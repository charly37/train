module BT()
{
aEpaisseurParoie = 1;
aDistanceInterieur = 18;
aLongeurInterieur = 46;
aTailleCapo = 40;

//Bas
translate(v = [0, -1, 0])
cube(size = [aDistanceInterieur+2*aEpaisseurParoie,aLongeurInterieur+1,aEpaisseurParoie], center = false);

//Fond
translate(v = [0,aLongeurInterieur, 0])
cube(size = [aDistanceInterieur+2*aEpaisseurParoie,aEpaisseurParoie,11], center = false);

//Cote
translate(v = [20-aEpaisseurParoie, 0, 0])
cube(size = [aEpaisseurParoie,aLongeurInterieur,10], center = false);
cube(size = [aEpaisseurParoie,aLongeurInterieur,10], center = false);

//Capot
translate(v = [0,aLongeurInterieur-aTailleCapo, 10])
cube(size = [aDistanceInterieur+2*aEpaisseurParoie,aTailleCapo,aEpaisseurParoie], center = false);

translate(v = [0, -1,1])
cube(size = [aDistanceInterieur+2*aEpaisseurParoie,1,1], center = false);

translate(v = [0,0, 2])
rotate(a=[0,90,0],r=0.9,$fn=100)
cylinder(h = aDistanceInterieur+2*aEpaisseurParoie, r=aEpaisseurParoie);
}

//BT();