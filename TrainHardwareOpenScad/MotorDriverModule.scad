module MotorDriver()
{
//dimentsion en mm

aHauteur = 4;
aLongeur = 22;
aLargeur = 10;

aEpaisseur = 1;

//base
cube(size = [aLongeur+2+2,aLargeur,aEpaisseur]);

cube(size = [2,aLargeur,aHauteur+aEpaisseur]);

//bloqueur au fond
translate(v = [aLongeur+2, 0, 0])
cube(size = [2,aLargeur,aEpaisseur*3]);

//haut
translate(v = [0, 0, aHauteur+aEpaisseur])
cube(size = [aLongeur+2,aLargeur,1]);
}
//MotorDriver();