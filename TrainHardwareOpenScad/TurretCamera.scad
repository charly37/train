module Sensor()
{
//real const
//aBaseCylDiam=4.7625;
aBaseCylDiam=4.7;
aCylHeight=10;
aEpaisseurBase=2;

//reference to static (blue cyl)
aDistCylnFront=aBaseCylDiam/2+5;
aDistBetweenCyl=25.4;
asDistBetweenCyl=12.7;

color( "Blue" )
translate([0,0,aEpaisseurBase])
cylinder(h = aCylHeight/2, r=aBaseCylDiam/2, $fs=0.05);

color( "Gray" )
translate([0,0+aDistBetweenCyl,aEpaisseurBase])
cylinder(h = aCylHeight/2, r=aBaseCylDiam/2, $fs=0.05);


color( "Red" )
translate([0+asDistBetweenCyl,0,aEpaisseurBase])
cylinder(h = aCylHeight/2, r=aBaseCylDiam/2, $fs=0.05);

color( "Orange" )
translate([0+asDistBetweenCyl,0+aDistBetweenCyl,aEpaisseurBase])
cylinder(h = aCylHeight/2, r=aBaseCylDiam/2, $fs=0.05);


color( "SpringGreen" )
translate([-aBaseCylDiam,-aBaseCylDiam,0])
cube(size = [asDistBetweenCyl+2*aBaseCylDiam,aDistBetweenCyl+2*aBaseCylDiam,aEpaisseurBase], center = false);


rotate([0,90,0])
translate([-1,-aBaseCylDiam,-4])
cube(size = [asDistBetweenCyl+2*aBaseCylDiam,aDistBetweenCyl+2*aBaseCylDiam,aEpaisseurBase], center = false);

}

Sensor();


