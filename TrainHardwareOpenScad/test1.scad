use <BTModule.scad>;
use <CameraModule.scad>;
use <MiniBoardModule.scad>;
use <MotorDriverModule.scad>;
use <Raspberry.scad>;
use <SensorModule.scad>;
use <TrainBase.scad>;
use <Sens.scad>;

translate([ 158, -20, 0 ])
cube(size = [1,40,1]);

translate([ 77, -20, 0 ])
cube(size = [1,40,1]);

translate([ 0, 4, 0 ])
cube(size = [165,1,2]);

translate([ 30, 4, 0 ])
rotate(a=[0,0,90])
MiniBoard();

translate([ 45, 4, 0 ])
rotate(a=[0,0,90])
MotorDriver();

translate([ 120, 2, 0 ])
rotate(a=[0,0,90])
Raspberry();

translate([ 140, 10, 0 ])
Sens();

translate([ 10, -5, 6 ])
{
    rotate(a=[180,0,0])
TrainBase();
}