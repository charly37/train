module Raspberry()
{
    // M2.5 pour les vis
    aDiamTrouVisse = 2.20;
    aHauteurCylindreTrouVis=10;
    aExtRoundDiam=5;
    aIntRoundDiam=1.5;
    aEspaceEntreBaseEtPi = 10;
    aDistanceEntreTrouLarg = 49;
    aDistanceEntreTrouLong = 58;

    cube(size = [aDistanceEntreTrouLong+2*aEspaceEntreBaseEtPi,aDistanceEntreTrouLarg+2*aEspaceEntreBaseEtPi,2]);
    translate([aEspaceEntreBaseEtPi,aEspaceEntreBaseEtPi,0]) 
    {
        difference()
        {
            cylinder(h = 7, d=aExtRoundDiam, $fs=1);
            cylinder(h = 9, d=aDiamTrouVisse, $fs=1);
        }
    }


    translate([aEspaceEntreBaseEtPi+aDistanceEntreTrouLong,aEspaceEntreBaseEtPi+aDistanceEntreTrouLarg,0]) 
    {
        difference()
        {
            cylinder(h = 7, d=aExtRoundDiam, $fs=1);
            cylinder(h = 9, d=aDiamTrouVisse, $fs=1);
        }
    }

    translate([aEspaceEntreBaseEtPi+aDistanceEntreTrouLong,aEspaceEntreBaseEtPi,0]) 
    {
        difference()
        {
            cylinder(h = 7, d=aExtRoundDiam, $fs=1);
            cylinder(h = 9, d=aDiamTrouVisse, $fs=1);
        }
    }

    translate([aEspaceEntreBaseEtPi,aEspaceEntreBaseEtPi+aDistanceEntreTrouLarg,0]) 
    {
        difference()
        {
            cylinder(h = 7, d=aExtRoundDiam, $fs=1);
            cylinder(h = 9, d=aDiamTrouVisse, $fs=1);
        }
    }


}
//Raspberry();