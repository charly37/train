module Sens()
{
aEpaisseurParoie = 1;
aDistanceInterieur = 22;
aLongeurInterieur = 14;
aHauteur=10;
aTailleCapo = 40;
aTmp=4;
difference(){
    
    union(){
        //Bas
        color("PaleTurquoise")
        cube(size = [aDistanceInterieur,aLongeurInterieur,aEpaisseurParoie], center = false);

        //Fond
        color("Red")
        translate(v = [0,10, aEpaisseurParoie])
        cube(size = [17,4,aHauteur], center = false);
    
        color("Chartreuse")
        translate(v = [0,9, aEpaisseurParoie])
        cube(size = [5,1,aHauteur], center = false);

        color("Fuchsia")
    
        translate(v = [aDistanceInterieur-aTmp,5, aEpaisseurParoie])
        cube(size = [aTmp,2,aHauteur], center = false);

        color("Cyan")
        translate(v = [0,1, aEpaisseurParoie])
        cube(size = [aDistanceInterieur,2,1], center = false);
        }

    rotate([90,0,0])
    translate(v = [3,4+aEpaisseurParoie, -20])
    cylinder(h=20, r=1,$fs=0.05);

    }
}

//Sens();