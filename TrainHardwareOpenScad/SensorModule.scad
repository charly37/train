module Sensor()
{
//real const
aCylDiam=2.70;
aBaseCylDiam=5;
aCylHeight=10;
aEpaisseurBase=2;

//reference to static (blue cyl)
aDistCylnFront=aBaseCylDiam/2+5;
aDistBetweenCyl=15;

color( "Blue" )
translate([0,0,aEpaisseurBase])
{
    difference()
    {
        cylinder(h = aCylHeight/2, r=aBaseCylDiam/2, $fs=0.05);
        cylinder(h = aCylHeight, r=aCylDiam/2, $fs=0.05);
    }
}

color( "Gray" )
translate([0,0+aDistBetweenCyl,aEpaisseurBase])
{
    difference()
    {
        cylinder(h = aCylHeight/2, r=aBaseCylDiam/2, $fs=0.05);
        cylinder(h = aCylHeight, r=aCylDiam/2, $fs=0.05);
    }
}


aLargeurBase = aDistBetweenCyl + 2*aBaseCylDiam;
color( "Red" )
translate([(aDistCylnFront),0,aEpaisseurBase])
cube(size = [7,aDistBetweenCyl,aCylHeight/2], center = false);

color( "SpringGreen" )
translate([-aBaseCylDiam,-aBaseCylDiam,0])
cube(size = [aDistCylnFront+aBaseCylDiam+7,aLargeurBase,aEpaisseurBase], center = false);

}

Sensor();


