aRailDiameter = 10;
aTrackLength = 210;

aTrackWidth = 50;

aDistanceBetweenSupport = 100;

module RailProfileToExtrude(){
circle(d=aRailDiameter, $fn=50);
   
translate([0,-aRailDiameter/4,0]) 
square([aTrackWidth,5]);
    
translate([aTrackWidth,0,0])
circle(d=aRailDiameter, $fn=50);
}

module RailProfileExtruded(){
linear_extrude(height = aTrackLength, center = false, convexity = 10, twist = 0)
RailProfileToExtrude();
}

module RailProfileExtrudedWithUnions(){
    RailProfileExtruded();
for (a =[0:2])
{    
echo(a);
aSuportX=10;

translate([aTrackWidth/2-aSuportX/2,0,a*aDistanceBetweenSupport])
cube([aSuportX,40,10]);
}
}

//the shape we are going to remove from the extruded rail
module ShapeToRemoveFromRail(){
    hull() {
        aRadius=20;
    translate([0,aDistanceBetweenSupport-aRadius-aRadius-50,0]) 
    cylinder(h=10, r=aRadius);
    cylinder(h=10, r=aRadius);

 }
}

module Final(){
    difference() {
RailProfileExtrudedWithUnions();

for (a =[1:2:3]){  
    translate([aTrackWidth/2,5,a*50]) 
    rotate([90,0,0])  
    ShapeToRemoveFromRail();
    }
}
}
Final();