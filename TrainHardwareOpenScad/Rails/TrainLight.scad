WIDTH=40;
HEIGHT=10;
LENGTH=300;
BEVEL=1;
TRACK_WIDTH=5;
TRACK_HEIGHT=2;
TRACK_DIST=20;
ROUND_R=400;


module track() {
    translate([WIDTH/2-TRACK_WIDTH-TRACK_DIST/2,HEIGHT-TRACK_HEIGHT,0])
    square([TRACK_WIDTH,TRACK_HEIGHT]);
    
    translate([WIDTH/2+TRACK_DIST/2,HEIGHT-TRACK_HEIGHT,0])
    square([TRACK_WIDTH,TRACK_HEIGHT]);
}

module body() {
    difference() {
        union(){
            translate([WIDTH/2-10/2,0])
            square([10,HEIGHT+40]);
            square([WIDTH,HEIGHT]);
        }
        translate([0,0,0.01])
        track();
    }
}

module brio_straight(len) {
    rotate([90,0,0])
    linear_extrude(height=len)
    body();
}


module brio_curved(r, parts) {
    translate([0,-r,0])
    difference() {
        rotate([0,0,-360/parts])
        difference() {
            rotate_extrude(convexity = 10, $fn=200)
            translate([r, 0, 0])
            difference() {
                body();
                translate([0,-HEIGHT+TRACK_HEIGHT,0])
                track();
            }
            translate([0,-500,-500])
            cube([1000,1000,1000]);
        }
        translate([-1000,-500,-500])
        cube([1000,1000,1000]);
    }
}


brio_straight(LENGTH);
translate([60,-60,0])
brio_curved(ROUND_R, 8); // inner radius, Nth part




