cylinder(h=200, r=5, center=false);
translate([50,0,0])
cylinder(h=200, r=5, center=false);

translate([0,0,10])
{
rotate([0,90,0])
cylinder(h=50, r=5, center=false);
translate([25,0,0])
rotate([90,0,0])
cylinder(h=10, r=5, center=false);
}


translate([0,0,100]) 
{
rotate([0,90,0])
cylinder(h=50, r=5, center=false);
translate([25,0,0])
rotate([90,0,0])
cylinder(h=10, r=5, center=false);
}

translate([0,0,190]) 
{
rotate([0,90,0])
cylinder(h=50, r=5, center=false);
translate([25,0,0])
rotate([90,0,0])
cylinder(h=10, r=5, center=false);
}