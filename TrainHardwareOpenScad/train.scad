aEpaisseurCote = 5;
aLongeurPiece = 40;
aHauteurRoue = 50;

$fa=0.05;
$fs=0.05;


module SupportRoue(){
    cylinder(h=10, d=10);
}

module Roue(){
    difference(){
        cylinder(h=5, d=15);
        translate([0,0,-1])
        cylinder(h=7, d=2);
    }
}

module BarrePassageRoue(){
    cylinder(h=60, d=3);
}

module Motor(){
    aEpaisseur = 3;
    aMoteurHauteur = 17;
    aLonguerMoteur = 20;
    cube([50+aEpaisseur,aLongeurPiece,aEpaisseur]);   
    
    translate([0,0,aMoteurHauteur+aEpaisseur])  
    cube([50+aEpaisseur,aLongeurPiece,aEpaisseur]);

    //les cotes
    cube([aEpaisseur,aLongeurPiece,aMoteurHauteur+aEpaisseur]);
    translate([50,0,0]) 
    cube([aEpaisseur,aLongeurPiece,aMoteurHauteur+aEpaisseur]);
    
    //support ineterne
    translate([aEpaisseur,0,aEpaisseur])
    cube([5,aLonguerMoteur,aEpaisseur]);  
    
    translate([aEpaisseur+5+25,0,aEpaisseur])
    cube([1,aLonguerMoteur,aEpaisseur]); 
    
    translate([aEpaisseur+15,0,aEpaisseur+15])
    cube([15,aLonguerMoteur,aEpaisseur]);
    
    translate([aEpaisseur,20,aEpaisseur])
    cube([25+5+1,2,17]);
    
    translate([aEpaisseurCote,3*aLongeurPiece/4,12])
    rotate([0,90,0])
    SupportRoue();
    
    translate([45,3*aLongeurPiece/4,12])
    rotate([0,90,0])
    SupportRoue();
}

module Result(){
difference(){

union(){  
    Motor();

    



//les cotes
cube([aEpaisseurCote,aLongeurPiece,60]);
translate([50,0,0]) 
cube([aEpaisseurCote,aLongeurPiece,60]);


   
//les supports de roue
    
translate([aEpaisseurCote,3*aLongeurPiece/4,aHauteurRoue])
rotate([0,90,0])
SupportRoue();


translate([40,3*aLongeurPiece/4,aHauteurRoue])
rotate([0,90,0])
SupportRoue();

translate([aEpaisseurCote,aLongeurPiece/4,aHauteurRoue])
rotate([0,90,0])
SupportRoue();


translate([40,aLongeurPiece/4,aHauteurRoue])
rotate([0,90,0])
SupportRoue();
}

translate([-1,aLongeurPiece/4,aHauteurRoue])
rotate([0,90,0])
BarrePassageRoue();

translate([-1,3*aLongeurPiece/4,aHauteurRoue])
rotate([0,90,0])
BarrePassageRoue();

translate([-1,3*aLongeurPiece/4,12])
rotate([0,90,0])
BarrePassageRoue();



}
}

Result();


translate([30,30,25])
rotate([0,0,0])
{
    translate([6,-15,-5])
    cylinder(h=35, d=1);
    translate([-6,-15,-5])
    cylinder(h=35, d=1);
    translate([0,-15,0])
    Roue();
    translate([0,-15,10])
    Roue();
    translate([0,-15,20])
    Roue();
    translate([0,-15,30])
    Roue();
}

