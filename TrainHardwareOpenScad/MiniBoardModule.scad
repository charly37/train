module MiniBoard()
{
//dimension in mm

aLong = 27;
aLarg = 17;
aHaut = 10;

aEpaisseurBord = 2;


difference(){
color("DarkTurquoise")
cube(size = [aLong+2*aEpaisseurBord,aLarg+2*aEpaisseurBord,aHaut+aEpaisseurBord]);

translate(v = [aEpaisseurBord, aEpaisseurBord, aEpaisseurBord])
color("LightGreen")
    //+1 pour que ca depasse
cube(size = [aLong,aLarg,aHaut+1]);
}
}
//MiniBoard();