#Motor Standby pin
CONST_PIN_OUT_STBY = 5
#Connect stop user input button
CONST_PIN_IN_STOP = 22
#Connect Motor board PWMA to pin 3
CONST_PIN_OUT_PWMA = 9
#Connect Motor Board AIN1 to pin 9 (direction info)
CONST_PIN_OUT_AIN1 = 17
#Connect Motor Board AIN2 to pin 8 (direction info)
CONST_PIN_OUT_AIN2 = 10
#Connect Front sensor on pin 7
CONST_PIN_IN_SENSOR1 = 27
#Connect Rear sensor on pin 6
CONST_PIN_IN_SENSOR2 = 23

#Turret Servo numbers
CONST_SERVO_HEIGHT = 1
CONST_SERVO_WIDTH = 0