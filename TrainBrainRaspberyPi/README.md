## Description - Raspberry Pi Train version
It offers a Web Hotspot "MyPi" on which you can connect. You can then go to the website http://192.168.10.1 to control the train

## Dependancies 

Raspbian:
https://www.raspberrypi.org/downloads/raspbian/

Rpi Cam interface: 
http://elinux.org/RPi-Cam-Web-Interface

ServoBlaster: 
https://github.com/richardghirst/PiBits

RPi.GPIO Python Module: 
http://sourceforge.net/p/raspberry-gpio-python/wiki/Home/
sudo apt-get install python-rpi.gpio

Tornado: 
http://www.tornadoweb.org/en/stable/
sudo apt-get install python-tornado

hostapd:
Wifi Hotspot
sudo apt-get install isc-dhcp-server hostapd

isc-dhcp-server:
DHCP server for the Wifi Hotspot