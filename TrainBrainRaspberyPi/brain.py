#sudo python
import RPi.GPIO as GPIO
import time
import sys
import tornado.ioloop
import tornado.web
import tornado.websocket
import logging
import ConstModule

class Handler_WS(tornado.websocket.WebSocketHandler):
    """Class handling the Web Socket requests"""

    def check_origin(self, origin):
        """MANDATORY otherwiase connection close auto"""
        return True
        
    def initialize(self, aTrainRef):
        self.aTrainRef = aTrainRef

    def open(self):
        """Methode call when the client and server has establish the connection"""
        logging.info('Connection open')
        self.write_message("connection opened")

    def on_message(self, iMessage):
        """Methode call when the server receive a message"""
        logging.info('Receive incoming message:'+str(iMessage))
        #self.write_message("toto")
        self.aTrainRef._cellAngles=str(iMessage)

    def on_close(self):
        """Methode call when the client and server close the connection"""
        logging.info('Connection close')


class Handler_Main(tornado.web.RequestHandler):
    """Class handling the Web requests"""

    def get(self):
        """Methode call when the server receive a GET HTTP request"""
        logging.info('Receive a GET request')
        #self.write("GET OK")
        logging.info('input is :'+str(input))
        self.render("index1.html")

class Handler_Argument_Main(tornado.web.RequestHandler):
    """Class handling the Web requests - to show how to handle args"""

    def initialize(self, aTrainRef):
        self.aTrainRef = aTrainRef

    def get(self, args):
        """Methode call when the server receive a GET HTTP request with args"""
        logging.info('Receive a GET request')
        #self.write("GET OK")
        logging.info('args is :'+str(args))
        if (str(args)=="START"):
            self.aTrainRef.start()
        elif (str(args)=="STOP"):
            self.aTrainRef.stop()
        elif (str(args)=="PLUS"):
            self.aTrainRef.faster()
        elif (str(args)=="MOINS"):
            self.aTrainRef.slower()
        elif (str(args)=="TUP"):
            self.aTrainRef.turretUp()
        elif (str(args)=="TDOWN"):
            self.aTrainRef.turretDown()
        elif (str(args)=="TRIGHT"):
            self.aTrainRef.turretRight()
        elif (str(args)=="TLEFT"):
            self.aTrainRef.turretLeft()
        else:
            logging.error('unknow arg :'+str(args))
        #self.render("index.html")
        self.write("ACK")

class Train:

    def __init__(self, iSpeed, iDirectionForward):
        self._speed = iSpeed;
        self._forwardDirection = iDirectionForward;
        self._isStart = False;
        self._servoBlasterFile = open('/dev/servoblaster', 'w')
        self._turretHeight = 50;
        self.sendPos(ConstModule.CONST_SERVO_HEIGHT,self._turretHeight)
        self._turretWidth = 50;
        self.sendPos(ConstModule.CONST_SERVO_WIDTH,self._turretWidth)
        GPIO.setup(ConstModule.CONST_PIN_OUT_PWMA, GPIO.OUT)
        self._pwm = GPIO.PWM(ConstModule.CONST_PIN_OUT_PWMA, 1000)
        self._pwm.ChangeDutyCycle(self._speed)
        self._pwm.start(self._speed)
        self._cellAngles=""

    def stop(self):
        logging.debug("Train stoping")
        self._isStart = False
        self.go()
        time.sleep(0.500)

    def isStop(self):
        return (not(self._isStart))
        
    def updateTurretFromScreenAngle(self):
        if (self._cellAngles!=""):
            #Update Gamma
            aGamma = self._cellAngles.split("_")[2]
            #logging.debug("Updating Gamma to : "+aGamma)
            aGammaF = float(aGamma)
            aGammaI = int(aGammaF)
            aGammaisNegative = False
            #logging.debug("Updating aGammaI to : "+str(aGammaI))
            if (aGammaI<0):
                #logging.debug("aGammaI --")
                aGammaI=(aGammaI*-1)-40
                aGammaisNegative = True
            else:
                #logging.debug("aGammaI ++")
                aGammaI=140-aGammaI
            if ((aGammaI>0)and(aGammaI<100)):
                #logging.debug("---------------aGammaI : "+str(aGammaI))
                self._turretHeight = 100 - aGammaI
                self.sendPos(ConstModule.CONST_SERVO_HEIGHT,self._turretHeight)
            #Update Alpha
            aAlpha = self._cellAngles.split("_")[0]
            logging.debug("Updating aAlpha to : "+aAlpha)
            aAlphaF = float(aAlpha)
            aAlphaI = int(aAlphaF)
            logging.debug("Updating aAlphaI to : "+str(aAlphaI))
            if (aGammaisNegative):
                logging.debug("gamma was neg")
                aAlphaI=(200 - aAlphaI)
            else:
                if ((aAlphaI>0) and (aAlphaI<30)):
                    logging.debug("aGammaI entre 0 30")
                    aAlphaI=30-aGammaI
                elif((aAlphaI>290) and (aAlphaI<360)):
                    logging.debug("aGammaI entre 0 30")
                    aAlphaI=30+(360-aGammaI)
                else:
                    logging.debug("aGammaI nul part")
            if ((aAlphaI>0)and(aAlphaI<100)):
                logging.debug("---------------aAlphaI : "+str(aAlphaI))
                self._turretWidth = aAlphaI
                self.sendPos(ConstModule.CONST_SERVO_WIDTH,self._turretWidth)
        
    def turretDown(self):
        self._turretHeight = min(self._turretHeight+1,100);
        self.sendPos(ConstModule.CONST_SERVO_HEIGHT,self._turretHeight)
        
    def turretUp(self):
        self._turretHeight = max(self._turretHeight-1,0);
        self.sendPos(ConstModule.CONST_SERVO_HEIGHT,self._turretHeight)
        
    def turretLeft(self):
        self._turretWidth = min(self._turretWidth+1,100);
        self.sendPos(ConstModule.CONST_SERVO_WIDTH,self._turretWidth)
        
    def turretRight(self):
        self._turretWidth = max(self._turretWidth-1,0);
        self.sendPos(ConstModule.CONST_SERVO_WIDTH,self._turretWidth)
        
    def sendPos(self,iServo, iPos):
        #echo 3=120 > /dev/servoblaster
        aCmd = str(iServo) + "=" + str(iPos) + "%\n"
        logging.debug("Updating servo : " + aCmd)
        self._servoBlasterFile.write(aCmd)
        self._servoBlasterFile.flush()
        #self._servoBlasterFile.close()
        
    def faster(self):
        self._speed=min(100,self._speed+5)
        self.go()
        
    def slower(self):
        self._speed=max(0,self._speed-5)
        self.go()

    def start(self):
        logging.debug("Train starting")
        self._isStart = True
        self.go()

    def go(self):
        logging.debug("Train go")
        if (self._isStart):
            logging.debug("Train moving with speed :" +str(self._speed))
            GPIO.output(ConstModule.CONST_PIN_OUT_STBY, True)
            aInPin1 = False
            aInPin2 = True

            if not(self._forwardDirection):
                aInPin1 = True
                aInPin2 = False

            GPIO.output(ConstModule.CONST_PIN_OUT_AIN1, aInPin1)
            GPIO.output(ConstModule.CONST_PIN_OUT_AIN2, aInPin2)
            #GPIO.output(ConstModule.CONST_PIN_OUT_PWMA, self._speed)
            self._pwm.ChangeDutyCycle(self._speed)
        else:
            GPIO.output(ConstModule.CONST_PIN_OUT_STBY, False)
            pass

    def revert(self):
        logging.debug("Train reverting")
        self.stop()
        self._forwardDirection = not(self._forwardDirection)
        time.sleep(0.300)
        self.start()

def foo(iTrain):
    #logging.info('Checking...')
    aFrontSensor = not(GPIO.input(ConstModule.CONST_PIN_IN_SENSOR1))
    aRearSensor = not(GPIO.input(ConstModule.CONST_PIN_IN_SENSOR2))
    if ((aFrontSensor == True) and (aRearSensor == True)):
        logging.info('Manual action to start/stop the train')
        if (aTrain.isStop()):
            aTrain.start()
        else:
            aTrain.stop()
    else:
        if ((aFrontSensor == True) or (aRearSensor == True)):
            logging.debug('Obstacle detected... revert !')
            aTrain.revert()
    iTrain.updateTurretFromScreenAngle()


if __name__ == "__main__":
    #Init
    logging.basicConfig(filename='example.log',level=logging.DEBUG,format='%(asctime)s %(levelname)s %(message)s')

    logging.info('Starting INIT')
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(ConstModule.CONST_PIN_OUT_AIN1, GPIO.OUT)
    GPIO.setup(ConstModule.CONST_PIN_OUT_AIN2, GPIO.OUT)
    #GPIO.setup(ConstModule.CONST_PIN_OUT_PWMA, GPIO.OUT)
    GPIO.setup(ConstModule.CONST_PIN_OUT_STBY, GPIO.OUT)

    #Set up input channel with pull-up control. Can be PUD_UP, PUD_DOWN or PUD_OFF (default)
    #GPIO.setup(7, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(ConstModule.CONST_PIN_IN_SENSOR1, GPIO.IN)
    GPIO.setup(ConstModule.CONST_PIN_IN_SENSOR2, GPIO.IN)

    aTrain = Train(50, True)
    #Mandatory for Tornado framework
    #We declare an application handling standard HTTP request with the Handler_Main class and also handling web socket with Handler_WS class
    aApplication = tornado.web.Application([(r"/", Handler_Main),(r'/static/(.*)',tornado.web.StaticFileHandler,{'path':'/home/pi/Web/'}),(r'/ws', Handler_WS, {"aTrainRef" : aTrain}),(r"/rest/([A-Z]+)", Handler_Argument_Main,{"aTrainRef" : aTrain})])
    logging.info('INIT is over')

    #Run
    #Init Tornado with our application
    aApplication.listen(80)
    #Start reactor
    tornado.ioloop.PeriodicCallback(lambda: foo(aTrain), 125).start()
    tornado.ioloop.IOLoop.instance().start()
