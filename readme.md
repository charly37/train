## Description
The train project repo. 

## Components 

* TrainBrainRaspberyPi : The core component which contains python/web code to control the train

* TrainHardwareOpenScad : The Hardware design (OpenScad) files to print the different parts
